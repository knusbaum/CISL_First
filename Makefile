CC = cc
LD = ld

all: compiler

as :
	$(CC) -ggdb -c -m32 test.s
	$(LD) -melf_i386 test.o

astest: as test.s
	@echo $(shell ./a.out; echo $$?)

compiler: io.c io.h lexer.h lexer.c parser.h parser.c codegen.h codegen.c main.c
	$(CC) -ggdb -Wall -Werror main.c lexer.c parser.c io.c codegen.h codegen.c 

clean: 
	-@rm *~
	-@rm *.o
	-@rm *.gch
	-@rm a.out
