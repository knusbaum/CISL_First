#ifndef CODE_GEN_H
#define CODE_GEN_H

void PostLabelC(char l);
void PostLabel(char * l);
void Header(void);
void Prolog(void);
void Epilog(void);
void Clear(void);
void Negate(void);
void LoadConstant(char * constant);
void LoadVarVal(char * name);
void LoadVar(char * name);
void LoadVarAddr(char * name);
void LoadVarInReg(void);
void Push(void);
void PushPtr(void);
void Pop(void);
void PopAdd(void);
void PopSub(void);
void PopMul(void);
void PopDiv(void);
void Store(char * name);
void StoreToPtrInEsp(void);
void GlobalInteger(char * name, char * val);
void GlobalString(char * name, char * val);
void CallFunction(char * name);
void BeginFunction(char * name);
void Return(void);
void DropParams(int count);
void PointerToVal(void);

// ##### Boolean #####

void Not(void);
void PopAnd(void);
void PopOr(void);
void PopXor(void);
void PopCompare(void);
void SetEqual(void);
void SetNEqual(void);
void SetGreater(void);
void SetLess(void);
void SetLessOrEqual(void);
void SetGreaterOrEqual(void);

//##### Conditional Constructs #####

void Branch(char * label);
void BranchFalse(char * label);

//##### IO Code #####

void ReadVar(void);
void WriteVar(void);
void IORoutines(void);
void WriteStr(char *s, char *label);
void StaticData(void);

#endif
