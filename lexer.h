#ifndef LEXER_H
#define LEXER_H

typedef enum {
    Identifier = 0,
    IfSym = 1,
    ElseSym = 2,
    VarSym = 3,
    DefSym = 4,
    WhileSym = 5,
    Integer,
    Operator,
    Str,
    END
} token_type_t;

#define TOKEN_LEN 50

//extern int Look;
extern char Token[TOKEN_LEN];
extern token_type_t Type; 
extern char * String;

void GetChar(void);
void GetAnyChar(void);
//void Match(char x);
void MatchString(char *s);
void GetName(void);
void GetNum(void);
void GetOp(void);
void GetKeyword(void);
void GetNext(void);
void Init(void);

int IsBoolOp(char c);
int IsAddOp(char *c);
int IsOrOp(char *c);
int IsAndOp(char *c);
int IsRelOp(char *c);

int IsMulOp(char *c);

#endif
