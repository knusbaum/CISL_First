#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "lexer.h"
#include "io.h"

int Look;
char TokenUpper[TOKEN_LEN];
char Token[TOKEN_LEN];
token_type_t Type;
char * keywords[] = { "IF", "ELSE", "VAR", "DEF", "WHILE", NULL };
char * String;

char * addops[] = { "+", "-", NULL };
char * mulops[] = { "*", "/", NULL };
char * relops[] = { "==", "!=", "<", ">", "<=", ">=", NULL };
char * andops[] = { "&&", NULL };
char * orops[] = { "||", "~", NULL };

char syms[] = "{}()+=-*/!><&|~;";

int Lookup(char ** table, char *val) {
    int i = 1;
    for(; *table != NULL; i++) {
        if( StrEq(*(table++), val) ) {
            return i;
        }
    }
    return 0;
}

int IsWhiteSpace(char c) {
    return (c == '\t' ||
            c == ' ' ||
            c == '\n');
}

int IsOrOp(char *c) {
    return Lookup(orops, c);
}

int IsAndOp(char *c) {
    return Lookup(andops, c);
}

int IsRelOp(char *c) {
    return Lookup(relops, c);
}

int IsMulOp(char *c) {
    return Lookup(mulops, c);
}

//int IsBoolOp(char *c) {
//    return ( IsOrOp(c) ||
//             IsAndOp(c) ||
//             IsRelOp(c) );
//}

int IsAddOp(char *c) {
    return Lookup(addops, c);
}

int IsOperator(char c) {
    return (strchr(syms, c) != NULL);
}

void GetAnyChar(void) {
    Look = getchar();
}

void GetChar(void) {
    GetAnyChar();
//    if(Look == EOF) Expected("something", "EOF");
}

void SkipWhite(void) {
    while(IsWhiteSpace(Look)) {
        GetChar();
    }
}

void Init(void) {
    GetChar();
}

void Match(char x) {
    if( Look == x ) {
        GetChar();
        SkipWhite();
    }
    else {
        ExpectedCC(x, Look);
    }
}

void MatchString(char *s) {
    if(!StrEq(s, Token)) Expected(s, Token);
    GetNext();
}

void GetSome(void) {
    if(!isalpha(Look)) ExpectedC("Alpha", Look);
    
    int i = 0;
    for(; isalnum(Look); i++) {
        Token[i] = Look;
        TokenUpper[i] = toupper(Look);
        GetChar();
    }
    Token[i] = 0;
    TokenUpper[i] = 0;
    Type = (token_type_t)Lookup(keywords, TokenUpper);
    SkipWhite();
//    fprintf(stderr, "Got: %s\n", Token);
}

void GetName(void) {
    GetSome();
    if(Type != Identifier) Expected("Name", Token);
}

void GetKeyword(void) {
    GetSome();
    if(Type < IfSym || Type > DefSym) Expected("Keyword", Token);
}

void GetNum(void) {
    if (! isdigit(Look)) ExpectedC("Integer", Look);
    
    int i = 0;
    for(; isdigit(Look); i++) {
        Token[i] = Look;
        TokenUpper[i] = Look;
        GetChar();
    }
    Token[i] = 0;
    TokenUpper[i] = 0;
    Type = Integer;
    SkipWhite();
//    fprintf(stderr, "Got: %s\n", Token);
}

void GetOp(void) {
    if(! IsOperator(Look)) ExpectedC("Operator", Look);
    
    int i = 0;
    for(; IsOperator(Look); i++) {
        Token[i] = Look;
        TokenUpper[i] = Look;
        GetChar();
        if(Token[i] == '(' || 
           Token[i] == '{' ||
           Token[i] == ')' ||
           Token[i] == '}') {
            i++;
            break;
        }
    }
    Token[i] = 0;
    TokenUpper[i] = 0;
    Type = Operator;
    SkipWhite();
//    fprintf(stderr, "Got: %s\n", Token);
}

void GetString(void) {
    Match('"');
    int max_len = 10;
    int currlen = 0;
    char * str = malloc(max_len * sizeof(char));
    
    while(Look != '"'){
        if(currlen == max_len - 1) {
            max_len *= 2;
            str = realloc(str, max_len * sizeof(char));
        }
        str[currlen++] = Look;
        GetChar();
    }
    Match('"');
    String = str;
    Type = Str;
}

void GetNext(void) {
    if(isalpha(Look)) {
//        printf("Getting some.");
        GetSome();
    }
    else if(isdigit(Look)) {
//        printf("Getting Num.");
        GetNum();
    }
    else if(IsOperator(Look)) {
//        printf("Getting Op.");
        GetOp();
    }
    else if(Look == EOF) {
        Type = END;
    }
    else if(Look == '"') {
        GetString();
    }
    else {
        Abort("Unexpected token: '%c'", Look);
    }
}

