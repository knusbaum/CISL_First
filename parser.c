#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "parser.h"
#include "io.h"
#include "lexer.h"
#include "codegen.h"

int LCount = 0;

typedef enum {
    Str_T,
    Int_T
} type_t;

typedef struct {
    char var[TOKEN_LEN];
    char val[TOKEN_LEN];
    char * str;
    type_t type;
} global_var_t;

global_var_t **vars = NULL;
int var_count = 0;
int max_count = 5;

void InitializeVars(void) {
    vars = malloc(max_count * sizeof(global_var_t *));
}

int VarDeclared(char * var) {
    int i;
    for(i = 0; i < var_count; i++) {
        if(StrEq(var, vars[i]->var)) {
            return 1;
        }
    }
    return 0;
}

void DeclVar(char * var, char * val) {

    if(VarDeclared(var)) {
        Abort("Var %s already declared.", var);
    }
 
    if(var_count == max_count) {
        max_count *= 2;
        vars = realloc(vars, max_count * sizeof(global_var_t *));
    }

    vars[var_count] = malloc(sizeof(global_var_t));
    strncpy(vars[var_count]->var, var, TOKEN_LEN);
    strncpy(vars[var_count]->val, val, TOKEN_LEN);
    vars[var_count]->str = NULL;
    vars[var_count]->type = Int_T;
    var_count++;
}

void DeclStrVar(char * var, char * val) {
    if(VarDeclared(var)) {
        Abort("Var %s already declared.", var);
    }
 
    if(var_count == max_count) {
        max_count *= 2;
        vars = realloc(vars, max_count * sizeof(global_var_t *));
    }

    vars[var_count] = malloc(sizeof(global_var_t));
    strncpy(vars[var_count]->var, var, TOKEN_LEN);
    memset(vars[var_count]->val, 0, TOKEN_LEN);
    vars[var_count]->str = val;
    vars[var_count]->type = Str_T;
    var_count++;
}

type_t VarType(char * var) {
    
    int i;
    for(i = 0; i < var_count; i++) {
        if(StrEq(var, vars[i]->var)) {
            return vars[i]->type;
        }
    }
    Abort("Var %s not declared.", var);
    return -1;
}

char * NewLabel() {
    char * l = malloc(9 * sizeof(char));
    snprintf(l, 9, "L%d", LCount++);
    return l;
}

void DoVarDecl(void) {
    GetName();
    char var[TOKEN_LEN];
    strncpy(var, Token, TOKEN_LEN);

    //fprintf(stderr, "Got Name: %s\n", var);

    GetNext();
    //fprintf(stderr, "Got Next: %s\n", Token);
    if(StrEq(Token, "=")) {
        MatchString("=");
        if(Type == Str) {
            DeclStrVar(var, String);
        }
        else if (Type == Integer) {
            DeclVar(var, Token);
        }
        else Abort("Unexpected Token.");
        
        GetNext();
    }
    else {
        DeclVar(var, "0");
    }
    MatchString(";");
}

void DoExpression(void);
int ParamList(void);

void DoIdentifier(void) {
    char var[TOKEN_LEN];
    strncpy(var, Token, TOKEN_LEN);
    
    GetNext();
    if(StrEq(Token, "(")) {
        MatchString("(");
        int paramCount = ParamList();
        MatchString(")");
        CallFunction(var);
        DropParams(paramCount);
    }
    else {
        type_t vartype = VarType(var);
        if(vartype == Str_T) {
            LoadVarAddr(var);
        }
        else if(vartype == Int_T) {
            LoadVar(var);
        }
    }
}

void DoOperator(void) {
    if(StrEq("*", Token)) {
        MatchString("*");
        LoadVarVal(Token);
        LoadVarInReg();
    }
    else if(StrEq("&", Token)) {
        MatchString("&");
        LoadVarAddr(Token);
    }
    GetNext();
}

void Factor(void) {
    if(StrEq(Token, "(")) {
        MatchString("(");
        DoExpression();
        MatchString(")");
    }
    else if(Type == Identifier) {
        DoIdentifier();
    }
    else if(Type == Integer) {
        LoadConstant(Token);
        GetNext();
    }
    else if(Type == Operator) {
        DoOperator();
    }
}

void FirstFactor(void) {
    if(StrEq(Token, "+")) {
        MatchString("+");
    }
    else if(StrEq(Token, "-")) {
        MatchString("-");
        Factor();
        Negate();
    }
    else {
        Factor();
    }
}

void Term(void);

void Multiply(void) {
    MatchString("*");
    Term();
    PopMul();
}

void Divide(void) {
    MatchString("/");
    Term();
    PopDiv();
}

void Rest(void) {
    while( IsMulOp(Token) ) {
        Push();
        if(StrEq("*", Token)) Multiply();
        else if(StrEq("/", Token)) Divide();
    }
}

void Term(void) {
    Factor();
    Rest();
}

void FirstTerm(void) {
    FirstFactor();
    Rest();
}

void Add(void) {
    MatchString("+");
    Term();
    PopAdd();
}

void Subtract(void) {
    MatchString("-");
    Term();
    PopSub();
}

void ArithExpression(void) {
    FirstTerm();
    while(IsAddOp(Token)) {
        Push();
        if (StrEq("+", Token)) Add();
        else if (StrEq("-", Token)) Subtract();
    }
}

void DoBoolFactor(void) {
    GetNum();
    LoadConstant(Token);
}

void DoLessThan(void) {
    MatchString("<");
    ArithExpression();
    PopCompare();
    SetLess();
}

void DoGreaterThan(void) {
    MatchString(">");
    ArithExpression();
    PopCompare();
    SetGreater();
}

void DoLessOrEqual(void) {
    MatchString("<=");
    ArithExpression();
    PopCompare();
    SetLessOrEqual();
}

void DoGreaterOrEqual(void) {
    MatchString(">=");
    ArithExpression();
    PopCompare();
    SetGreaterOrEqual();
}

void DoEqual(void) {
    MatchString("==");
    ArithExpression();
    PopCompare();
    SetEqual();
}

void DoNotEqual(void) {
    MatchString("!=");
    ArithExpression();
    PopCompare();
    SetNEqual();
}

void Relation(void) {
    ArithExpression();
    while(IsRelOp(Token)) {
        Push();
        if(StrEq("<", Token)) {
            DoLessThan();
        }
        else if(StrEq(">", Token)) {
            DoGreaterThan();
        }
        else if(StrEq("<=", Token)) {
            DoLessOrEqual();
        }
        else if(StrEq(">=", Token)) {
            DoGreaterOrEqual();
        }
        else if(StrEq("==", Token)) {
            DoEqual();
        }
        else if(StrEq("!=", Token)) {
            DoNotEqual();
        }
        else Expected("Boolean operator", Token);
    }
}

void NotFactor(void) {
    if( StrEq(Token,"!")) {
        MatchString("!");
        Relation();
        Not();
    }
    else {
        Relation();
    }
}

void DoAnd(void) {
    MatchString("&");
    NotFactor();
    PopAnd();
}

void DoBoolTerm(void) {
    NotFactor();
    while(IsAndOp(Token)) {
        Push();
        if(StrEq(Token, "&")) DoAnd();
    }
}

void DoOr(void) {
    MatchString("|");
    DoBoolTerm();
    PopOr();
}

void DoXor(void) {
    MatchString("~");
    DoBoolTerm();
    PopXor();
}

void DoBoolean(void) {
    DoBoolTerm();
    while(IsOrOp(Token)) {
        Push();
        if( StrEq(Token, "|")) DoOr();
        else if (StrEq(Token, "~")) DoXor();
    }
}

void DoBlock(void);

int ParamList(void) {
    if(StrEq(Token, ")")) return 0;
    int params = 1;
    EmitLnT("## Doing boolean.");
    DoBoolean();
    EmitLnT("## Pushing.");
    Push();
    while(StrEq(Token, ",")) {
        MatchString(",");
        EmitLnT("## Doing boolean.");
        DoBoolean();
        EmitLnT("## Pushing.");
        Push();
        params++;
    }
    EmitLnT("## Done.");
    return params;
}

void DoIf(void) {
    char * l1 = NewLabel();
    char * l2 = l1;
    
    GetNext();
    MatchString("(");
    
    DoBoolean();
    MatchString(")");

    BranchFalse(l1);
    DoBlock();
    if(Type == ElseSym) {
        GetNext();
        l2 = NewLabel();
        Branch(l2);
        PostLabel(l1);
        DoBlock();
    }
    PostLabel(l2);

    if(l2 != l1) {
        free(l2);
    }
    free(l1);
}

void DoWhile(void) {
    GetNext();
    
    char * lab1 = NewLabel();
    char * lab2 = NewLabel();
   
    PostLabel(lab1);
    MatchString("(");
    DoBoolean();
    MatchString(")");
    BranchFalse(lab2);
    
    DoBlock();
    Branch(lab1);
    PostLabel(lab2);
}

void DoExpression(void) {
    if(StrEq(Token, "{")) {
        DoBlock();
    }
    else {
        if(Type == IfSym) {
            DoIf();
        }
        else if(Type == WhileSym) {
            DoWhile();
        }
        else {
            DoBoolean();
            
            if(StrEq(Token, "=")) {
                MatchString("=");
                PushPtr();
                DoBoolean();
                StoreToPtrInEsp();
            }
            else {
                PointerToVal();
            }

            MatchString(";");
        }
    }
}

void DoBlock(void) {
    MatchString("{");
    while(! StrEq(Token, "}")) {
        DoExpression();
    }
    GetNext();
}

void DoFunDecl(void) {
    GetName();
    BeginFunction(Token);
    GetNext();
    MatchString("(");
    MatchString(")");
    DoBlock();
    Return();
}

void ProduceVars(void) {
    int i;
    EmitLnT("## Global Data Declarations ##");
    EmitLnT(".data");
    for(i = 0; i < var_count; i++) {
        if(vars[i]->type == Str_T) {
            GlobalString(vars[i]->var, vars[i]->str);
        }
        else {
            GlobalInteger(vars[i]->var, vars[i]->val);
        }
        free(vars[i]);
    }
    free(vars);
}

void Program(void) {
    InitializeVars();
    
    Header();
    Prolog();
    CallFunction("main");
    Epilog();

    GetNext();
    
    while(Type != END) {
        //GetKeyword();
        switch(Type) {
        case VarSym: DoVarDecl(); break;
        case DefSym: DoFunDecl(); break;
        default: Expected("Var or Def", Token);
        }
    }
    
    IORoutines();
    ProduceVars();
}
