	## TINY Program ##
	.global _start


	## Start of Executable ##
	.text
_start:
	call main

	## Epilog ##
__end:
	movl %eax, %ebx
	movl $1, %eax
	int $0x80
	## END TINY Program ##

	## Function f ##
f:
	pushl %ebp
	movl %esp, %ebp
	leal x, %eax
	pushl %eax
	leal x, %eax
	movl (%eax), %eax
	pushl %eax
	movl $1, %eax
	movl %eax, %ebx
	popl %eax
	subl %ebx, %eax
	popl %ebx
	movl %eax, (%ebx)
	movl %ebp, %esp
	popl %ebp
	ret

	## Function main ##
main:
	pushl %ebp
	movl %esp, %ebp
L0:
	leal x, %eax
	movl (%eax), %eax
	pushl %eax
	movl $0, %eax
	popl %ebx
	cmpl %ebx, %eax
	movl $0, %eax
	setl %al
	test %eax, %eax
	jz L1
	leal z, %eax
	pushl %eax
	leal y, %eax
	popl %ebx
	movl %eax, (%ebx)
	## Doing boolean.
	leal z, %eax
	## Pushing.
	movl (%eax), %eax
	pushl %eax
	## Done.
	call writestr
	addl $4, %esp
	call f
	addl $0, %esp
	jmp L0
L1:
	movl %ebp, %esp
	popl %ebp
	ret

	## IO Routines ##

read:
	 pushl %ebp
	 movl %esp, %ebp

	 subl $26, %esp
	 movl $0x03, %eax
	 movl $0x00, %ebx
	 movl %esp, %ecx
	 movl $25, %edx
	 int  $0x80

	 cmpl $0, %eax
	 je __io_error

	 movl %eax, %ecx
	 decl %ecx
	 xorl %eax, %eax
	 xorl %ebx, %ebx
	 xorl %esi, %esi

_read_loop:
	 cmpl %esi, %ecx
	 je _read_end

	 movb (%esp,%esi), %bl
	 subl $'0', %ebx

	# Only allow numeric characters
	 cmpl $9, %ebx
	 jg __numeric_error

	 movl $10, %edx
	 mull %edx
	 addl %ebx, %eax
	 incl %esi
	 jmp _read_loop

_read_end:
	 movl 8(%ebp), %ecx
	 movl %eax, (%ecx)
	 movl %ebp, %esp
	 popl %ebp
	 ret

write:
	 pushl %ebp
	 movl %esp, %ebp

	 movl 8(%ebp), %eax
	 xorl %ecx, %ecx

	 subl $1, %esp
	 movb $'\n', (%esp)
	 subl $1, %esp
	 movb $'\r', (%esp)

_write_loop:
	 test %eax, %eax
	 jz _write_end

	 movl $10, %ebx
	 xorl %edx, %edx
	 divl %ebx
	 addl $'0', %edx
	 subl $1, %esp
	 movb %dl, (%esp)
	 incl %ecx
	 jmp _write_loop
_write_end:

	 addl $2, %ecx
	 movl %ecx, %edx
	 movl $0x04, %eax
	 movl $0x01, %ebx
	 movl %esp, %ecx
	 int $0x80

	 cmpl $-1, %eax
	 je __io_error

	 movl %ebp, %esp
	 popl %ebp
	 ret

writestr:
	 pushl %ebp
	 movl %esp, %ebp

	 movl 8(%ebp), %edi
	 movl $-1, %ecx
	 movl $-1, %edx
	 movl $0, %eax
	 repne scasb
	 subl %ecx, %edx

	 movl 8(%ebp), %ecx
	 movl $0x04, %eax
	 movl $0x01, %ebx
	 int $0x80

	 cmpl $-1, %eax
	 je __io_error

	 movl %ebp, %esp
	 popl %ebp
	 ret


__io_error:
	 leal _err_io_msg, %eax
	 pushl %eax
	 call writestr
	 jmp __end


__numeric_error:
	 leal _err_numeric_msg, %eax
	 pushl %eax
	 call writestr
	 jmp __end

	## Runtime Data ##
	.data
_err_numeric_msg:	.ascii "\nFATAL: You must input integers.\n\0"
_err_io_msg:	.ascii "\nFATAL: IO Error.\n\0"
	## Global Data Declarations ##
	.data
x:	.int 10
y:	.ascii "Hello, World!\n\0"
z:	.int 0
